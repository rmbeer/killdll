#ifndef __KILLDLL_DLL_H__
#define __KILLDLL_DLL_H__
#include <stdint.h>
#include "MZ.h"

class DLL{
public:
  DLL();
  ~DLL();
  //Archivo
  void _Free();
  int Open(char*);
  void Close();
  void ShowHead();
  char*hnd;
  size_t thnd;
  EXE*head;
  EXEPE*hp;
  //Simbolos
  void*GetSim(char*);
  //Tabla de saltos
  off_t GetPosJmp(unsigned int);
  uint32_t GetValJmp(unsigned int);
  struct{
    off_t p;
    uint32_t v;
  }*tbljmp;
  unsigned int ttbljmp;
  //Datos extras
  size_t TamBin;
  off_t IniData;
  off_t RelocTbl;
  unsigned int RelocTot;
};

#endif
