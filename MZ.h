#ifndef __KILLDLL_MZ_H__
#define __KILLDLL_MZ_H__

struct EXE {
  unsigned short signature; /* == 0x5a4D */
  unsigned short bytes_in_last_block;
  unsigned short blocks_in_file;
  unsigned short num_relocs;
  unsigned short header_paragraphs;
  unsigned short min_extra_paragraphs;
  unsigned short max_extra_paragraphs;
  unsigned short ss;
  unsigned short sp;
  unsigned short checksum;
  unsigned short ip;
  unsigned short cs;
  unsigned short reloc_table_offset;
  unsigned short overlay_number;
  unsigned long OffPE;
};

struct EXE_RELOC {
  unsigned short offset;
  unsigned short segment;
};

struct EXEPE {
  // Cabecera COFF
  uint32_t signature; /* == 0x00004550 */
  uint16_t machine;
  uint16_t numsecc;
  uint32_t timedatestamp;
  uint32_t ptrsymboltbl;
  uint32_t totsymboltbl;
  uint16_t tamoptionhead;
  uint16_t caracteristicas;
  // CABECERAS OPCIONALES
  // Campos Estandar COFF
  struct EstCOFF{
    uint16_t magic;
    uint16_t ver;
    uint32_t tamcode;
    uint32_t taminitdata;
    uint32_t tamuninitdata;
    uint32_t ptrentrypoint;
    uint32_t baseofcode;
    uint32_t baseofdata;
  }
  // Campos Especificaciones Windows
  struct EspWin{
    uint32_t imagebase;
    uint32_t secalign;
    uint32_t filealign;
    uint32_t verSO;
    uint32_t verimage;
    uint32_t versubsystem;
    uint32_t verwin32;
    uint32_t tamimage;
    uint32_t tamhead;
    uint32_t checksum;
    uint16_t subsystem;
    uint16_t dllcaracteristicas;
    uint32_t tamstackreserv;
    uint32_t tamstackcommit;
    uint32_t tamheapreserv;
    uint32_t tamheapcommit;
    uint32_t loadflags;
    uint32_t numrvaandsize;
  }
  // Datos Directorios
  struct DataDir{
    uint32_t tblexport;
    uint32_t ttblexport;
    uint32_t tblimport;
    uint32_t ttblimport;
    uint32_t tblresource;
    uint32_t ttblresource;
    uint32_t tblexcep;
    uint32_t ttblexcep;
    uint32_t tblcert;
    uint32_t ttblcert;
    uint32_t tblbreloc;
    uint32_t ttblbreloc;
    uint32_t debug;
    uint32_t tdebug;
    uint32_t archdata;
    uint32_t tarchdata;
    uint32_t globalptr;
    uint32_t BSR;
    uint32_t tbltls;
    uint32_t ttbltls;
    uint32_t tblloadconfig;
    uint32_t ttblloadconfig;
    uint32_t boundimport;
    uint32_t tboundimport;
    uint32_t tblimportaddr;
    uint32_t ttblimportaddr;
    uint32_t dlyimportdesc;
    uint32_t tdlyimportdesc;
    uint32_t clrruntimeheader;
    uint32_t tclrruntimeheader;
    uint32_t BSR2,BSR3;
  } // FIN CABECERAS OPCIONALES
  // Seccion Tabla
  struct SecTbl{
    uint64_t Nombre;
    uint32_t tamvirtual;
    uint32_t ptrvirtual;
    uint32_t totrawdata;
    uint32_t ptrrawdata;
    uint32_t ptrreloc;
    uint32_t ptrlinenumb;
    uint16_t totreloc;
    uint16_t totlinenumb;
    uint32_t caracteristicas;
  }
};

#endif
