#include <sys/stat.h>
#include <unistd.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include "dll.hpp"

DLL::DLL(){hnd=0;tbljmp=0;
}
DLL::~DLL(){
  Close();
}

int DLL::Open(char*zfl){
  {struct stat st;
    if(stat(zfl,&st))return -1;
    if((st.st_mode&S_IFMT)!=S_IFREG)return -1;
    thnd=st.st_size;
  }
  int fl=open(zfl,O_RDONLY);if(!fl)return -1;
  hnd=(char*)malloc(thnd);if(!hnd){close(fl);return -1;}
  read(fl,hnd,thnd);
  close(fl);
  head=(EXE*)hnd;
  if(head->signature!=0x5a4d){Close();return -1;}
  TamBin=head->blocks_in_file*512-(head->bytes_in_last_block)?(512-head->bytes_in_last_block):0;
  IniData=head->header_paragraphs<<4;
  RelocTbl=head->reloc_table_offset;
  RelocTot=head->num_relocs;
  hp=(EXEPE*)head->OffPE;
  return 0;
}

void DLL::Close(){if(hnd){free(hnd);hnd=0;}_Free();}

void DLL::_Free(){
  if(tbljmp){free(tbljmp);tbljmp=0;}
}

void DLL::ShowHead(){
  printf("Checksum: %2x\n",head->checksum);
  printf("Tam.Real: %d\n",TamBin);
  printf("Off Datos: %d(%4x)\n",IniData,IniData);
  printf("Off Tabla de Relocalizacion: %d(%4x)\n",RelocTbl,RelocTbl);
  printf("Total de Relocalizaciones: %d\n",RelocTot);
  printf("SS: %4x - SP: %4x\n",head->ss,head->sp);
  printf("CS:IP - %4x:%4x\n",head->cs,head->ip);
  printf("\nNumero de Superposicion: %4x\n",head->overlay_number);
}

/** TOMAR DIRECCION DE SIMBOLO
 * @param char* Nombre Simbolo
 * @return 0=ERR / !=Ptr del Simbolo
 */
void*DLL::GetSim(char*NomSim){
  void*ptr;
  return ptr;
}

off_t DLL::GetPosJmp(unsigned int np){
  if(np>=ttbljmp)return -1;
  return tbljmp[np].p;
}

uint32_t DLL::GetValJmp(unsigned int np){
  if(np>=ttbljmp)return -1;
  return tbljmp[np].v;
}
