#include <stdio.h>
#include "dll.hpp"

int main(int targc,char**argc){
  int i;
  DLL dll;
  if(targc==1){
    printf("%s [Nombre Archivo]\n",argc[0]);
  }else if(targc!=2){
    printf("Demasiados parametros.\n");
    return -1;
  }
  if(dll.Open(argc[1])){printf("Falla abriendo Archivo.\n");return -1;}
  dll.ShowHead();
  dll.Close();
  return 0;
}
