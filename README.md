Kill DLL

	Destroza, tritura, procesa, y lo hace bosta para devolver las distintas partes que compone un DLL. Con esta libreria podes extraer codigos, tabla de saltos, lista de simbolos, llamados a simbolos, relocalizaciones, etc.
	Informa los datos de cada una de las partes o te devuelve los datos en el codigo de C++ para utilizarlo con tu programa.

	Por si fuera poco, puede incluso procesar codigos para ser utilizado en el tuyo como si fuera una subrutina apartada.

	Es para los desarrolladores y funciona especialmente en GNU/Linux donde no hay ningun acceso a los DLLs, EXEs y otras extensiones. Solo para uso personal con su propia libreria o aplicacion portable.

PLANES:
- Hacerlo compatible con librerias de 16, 32 y 64 bits.
- Extender el uso al resto de PE.
- Hacer deteccion de los distintos formatos de DLL.

ENGLISH ========================================================================

	Shred, processed, and craped to return the various parts of a DLL.  With this library you can extract codes, jump tables, list of symbols, called symbols, relocations, etc.
	Informs the data of each of the parts or returns the data in C++ code to use with your program.

	As if that were not enough, you can even process codes to be used in yours as if it were a remote subroutine.

	It is for developers and works especially in GNU/Linux where there is no access to DLLs, EXEs and other extensions.  Only for personal use with its own library or portable application.

PLANS:
- Make it compatible with libraries of 16, 32 and 64 bits.
- Extend the use to the rest of PE.
- Make detection of different DLL formats.

Documentaciones con Licencia CC: BY-NC-SA
Codigos Fuentes con Licencia GPL
